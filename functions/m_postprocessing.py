"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    values_1=np.empty(len(x))
    for i in range(len(x)):
        square=pow(x[i],2)+pow(y[i],2)+pow(z[i],2)
        values_1[i]=np.sqrt(square)
        
    return(values_1)

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """


    
    if len(time) != len(data):
        raise ValueError("Input arrays 'time' and 'data' must have the same length.")

   
    interpolation_points = np.linspace(min(time), max(time), num=len(time))

    
    interpolated_values = np.interp(interpolation_points, time, data)

    return interpolation_points, interpolated_values


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    if len(x) != len(time):
        raise ValueError("Input arrays 'x' and 'time' must have the same length.")

    
    sampling_frequency = 1 / np.mean(np.diff(time))
    fft_result = np.fft.fft(x)
    frequency_axis = np.fft.fftfreq(len(x), d=1/sampling_frequency)
    positive_frequencies = frequency_axis > 0
    amplitude_spectrum = 2.0 / len(x) * np.abs(fft_result[positive_frequencies])
    positive_frequencies_values = frequency_axis[positive_frequencies]

    return positive_frequencies_values, amplitude_spectrum