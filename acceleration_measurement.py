import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_washingmachine.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sensorid= list(setup_json_dict)[2]


acceleration_x=[]
acceleration_y=[]
acceleration_z=[]
timestamp=[]
diction = {
    sensorid:{"acceleration_x":{"value":[],"unit":[]},"acceleration_y":{"value":[],"unit":[]},"acceleration_z":{"value":[],"unit":[]},"timestamp":{"value":[],"unit":[]}}
}



# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
import re
t0=time.time()
i2c = board.I2C()  # uses board.SCL and board.SDA
accelerometer = adafruit_adxl34x.ADXL345(i2c)
t2=0
while t2 <= measure_duration_in_s:
    xyz=("%f %f %f"%accelerometer.acceleration)
    numbers=[float(match.group())for match in re.finditer(r'\d+(\.\d+)?',xyz)]
    acceleration_x.append(numbers[0])
    acceleration_y.append(numbers[1])
    acceleration_z.append(numbers[2])
    timestamp.append(t2)
    time.sleep(0.001)
    t1=time.time()
    t2=t1-t0

diction[sensorid]["acceleration_x"]["value"]=acceleration_x
diction[sensorid]["acceleration_y"]["value"]=acceleration_y
diction[sensorid]["acceleration_z"]["value"]=acceleration_z
diction[sensorid]["timestamp"]["value"]=timestamp

unit=[]
for i in range(len(diction[sensorid]["acceleration_x"]["value"])):
    unit.append("m/s²")
diction[sensorid]["acceleration_x"]["unit"]=unit
unit=[]
for i in range(len(diction[sensorid]["acceleration_y"]["value"])):
    unit.append("m/s²")
diction[sensorid]["acceleration_y"]["unit"]=unit
unit=[]
for i in range(len(diction[sensorid]["acceleration_z"]["value"])):
    unit.append("m/s²")
diction[sensorid]["acceleration_z"]["unit"]=unit
unit=[]
for i in range(len(diction[sensorid]["timestamp"]["value"])):
    unit.append("s")
diction[sensorid]["timestamp"]["unit"]=unit
print(diction)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
with h5py.File(path_h5_file,'w') as file:
    def convertandstore(group, data):
        for key,value in data.items():
            if isinstance(value,dict):
                subgroup=group.create_group(key)
                convertandstore(subgroup,value)
            elif isinstance(value,np.ndarray) and value.dtype.kind =='S':
                group.create_dataset(key, data=value, dtype='S')
            else:
                group.create_dataset(key, data=value)
    convertandstore(file,diction)
            



# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
